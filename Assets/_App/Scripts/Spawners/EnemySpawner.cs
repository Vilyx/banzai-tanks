﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField]
        private GameObject enemyPrefab = null;

		public GameObject Spawn()
		{
			var enemy = Instantiate(enemyPrefab);
			enemy.transform.position = transform.position;
			enemy.transform.rotation = transform.rotation;
			return enemy;
		}
	}
}