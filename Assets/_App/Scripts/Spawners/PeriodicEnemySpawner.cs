﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
	public class PeriodicEnemySpawner : MonoBehaviour
	{
		[SerializeField]
		private GameObject enemyPrefab = null;
		[SerializeField]
		private float minSpawnPeriod = 10;
		[SerializeField]
		private float maxSpawnPeriod = 20;

		private bool spawningStarted = false;

		private float nextSpawnTime = 0;

		private void Update()
		{
			if (!spawningStarted)
				return;
			if (Time.time > nextSpawnTime)
			{
				RecalcNextSpawnTime();
				var enemy = Instantiate(enemyPrefab);
				enemy.transform.position = transform.position;
				enemy.transform.rotation = transform.rotation;
			}
		}

		public void StartSpawning()
		{
			spawningStarted = true;
			RecalcNextSpawnTime();
		}

		public void StopSpawning()
		{
			spawningStarted = false;
		}

		private void RecalcNextSpawnTime()
		{
			nextSpawnTime = Time.time + UnityEngine.Random.Range(minSpawnPeriod, maxSpawnPeriod);
		}
	}
}