﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace App
{
	public class TankSpawner : MonoBehaviour
	{
		[SerializeField]
		private GameObject tankPrefab = null;

		public GameObject Spawn()
		{
			var tankGO = Instantiate(tankPrefab);
			tankGO.transform.position = transform.position;
			tankGO.transform.rotation = transform.rotation;
			return tankGO;
		}
	} 
}
