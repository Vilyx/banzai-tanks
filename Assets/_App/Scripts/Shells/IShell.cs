﻿namespace App
{
	public interface IShell
	{
		IDamageable Owner { get; set; }
	}
}