﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
	[RequireComponent(typeof(Rigidbody))]
	public class Shell : MonoBehaviour, IShell
	{
		[SerializeField]
		private int damageAmount = 10;
		[SerializeField]
		private GameObject explosion = null;
		private Rigidbody shellRigidbody;
		private MeshRenderer meshRenderer;
		private float lifeDuration = 10;

		public IDamageable Owner { get; set; }

		private void Awake()
		{
			shellRigidbody = GetComponent<Rigidbody>();
			meshRenderer = GetComponent<MeshRenderer>();
			explosion.SetActive(false);

			StartCoroutine(DieOfElderness());
		}

		private void OnTriggerEnter(Collider other)
		{
			var damageable = other.GetComponent<IDamageable>();
			if (damageable != null)
			{
				if (damageable == Owner)
				{
					return;
				}
				else
				{
					damageable.ApplyDamage(damageAmount);
				}
			}

			shellRigidbody.isKinematic = true;
			shellRigidbody.detectCollisions = false;
			meshRenderer.enabled = false;
			explosion.SetActive(true);
			var particles = explosion.GetComponentsInChildren<ParticleSystem>();
			for (int i = 0; i < particles.Length; i++)
			{
				particles[i].Play();
			}
			var audio = explosion.GetComponentInChildren<AudioSource>();
			audio.Play();
			Destroy(gameObject, 2);
		}

		private IEnumerator DieOfElderness()
		{
			yield return new WaitForSeconds(lifeDuration);
			Destroy(gameObject);
		}
	}
}