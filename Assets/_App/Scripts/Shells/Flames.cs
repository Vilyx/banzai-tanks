﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
	public class Flames : MonoBehaviour, IShell
	{
		[SerializeField]
		private int damageAmount = 10;
		private float lifeDuration = 2;
		public IDamageable Owner { get; set; }

		private void Awake()
		{
			StartCoroutine(DieOfElderness());
		}

		private void OnTriggerEnter(Collider other)
		{
			var damageable = other.GetComponent<IDamageable>();
			if (damageable != null)
			{
				if (damageable == Owner)
				{
					return;
				}
				else
				{
					damageable.ApplyDamage(damageAmount);
				}
			}
		}

		private IEnumerator DieOfElderness()
		{
			yield return new WaitForSeconds(lifeDuration);
			Destroy(gameObject);
		}
	} 
}
