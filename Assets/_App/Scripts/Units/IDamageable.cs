﻿using System;

namespace App
{
	public interface IDamageable
	{
		bool Alive { get; }
		int Health { get; }
		int MaxHealth { get; }

		event Action<IDamageable> died;
		event Action<int> healthChanged;

		void ApplyDamage(int damageAmount);
	}
}