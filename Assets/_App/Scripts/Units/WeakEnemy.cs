﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace App
{
	[RequireComponent(typeof(NavMeshAgent))]
	[RequireComponent(typeof(Collider))]
	public class WeakEnemy : MonoBehaviour, IDamageable
	{
		public event Action<IDamageable> died;
		public event Action<int> healthChanged;

		[SerializeField]
		private int attack = 20;
		[SerializeField]
		private int maxHealth = 20;
		[Range(0.0f, 1.0f)]
		[SerializeField]
		private float armour = 0.2f;

		private int health;
		private Animator animator;
		private Tank target;
		private NavMeshAgent navAgent;
		private Collider characterCollider;

		private float lastTargetUpdate = 0;
		private float targetUpdatePeriod = 0.1f;

		public bool Alive { get; private set; }

		public int Health
		{
			get => health;
			private set
			{
				health = value;
				healthChanged?.Invoke(health);
			}
		}

		public int MaxHealth
		{
			get => maxHealth;
		}

		private void Awake()
		{
			animator = GetComponentInChildren<Animator>();
			navAgent = GetComponent<NavMeshAgent>();
			characterCollider = GetComponent<Collider>();
		}

		private void OnEnable()
		{
			target = FindObjectOfType<Tank>();
			navAgent.enabled = true;
			Alive = true;
			Health = maxHealth;
			animator.SetBool("Alive", true);
			characterCollider.enabled = true;
		}

		private void Update()
		{
			if (target == null)
			{
				Destroy(gameObject);
			}
			if (Alive && Time.time - lastTargetUpdate > targetUpdatePeriod)
			{
				lastTargetUpdate = Time.time;
				navAgent.destination = target.transform.position;
			}
		}

		private void OnCollisionEnter(Collision collision)
		{
			if (collision.collider.attachedRigidbody == null || !Alive)
				return;
			if (collision.collider.attachedRigidbody.transform == target.transform)
			{
				BattleHistory.AddRecord(HistoryRecordType.ENEMY_DIED);
				Destroy(gameObject);
				target.ApplyDamage(attack);
			}
		}

		public void ApplyDamage(int damageAmount)
		{
			if (Alive)
			{
				Health = Mathf.Max(Health - Mathf.FloorToInt(damageAmount * (1 - armour)), 0);
				animator.SetTrigger("Hit");
				if (Health == 0)
				{
					BattleHistory.AddRecord(HistoryRecordType.ENEMY_KILLED);
					navAgent.enabled = false;
					animator.SetBool("Alive", false);
					Alive = false;
					characterCollider.enabled = false;
					died?.Invoke(this);
					Destroy(gameObject, 5);
				}
			}
		}
	}
}