﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

namespace App
{
	public class Tank : MonoBehaviour, IDamageable
	{
		public event Action<IDamageable> died;
		public event Action<int> healthChanged;

		[SerializeField]
		private InputReader inputReader = null;
		[SerializeField]
		private Transform turretsHolder = null;
		[SerializeField]
		private List<TankTurret> turrets = null;
		[SerializeField]
		private float movementSpeed = 5;
		[Range(0.0f,1.0f)]
		[SerializeField]
		private float armour = 0.2f;

		[SerializeField]
		private int maxHealth = 100;
		private int health;
		private int currentTurret = 0;

		private Rigidbody tankRigidbody;
		private Vector3 tankVelocity;

		public Vector2 ControlVector { get; private set; }
		public bool Alive { get; private set; }
		public int Health
		{
			get => health; 
			private set{
				health = value;
				healthChanged?.Invoke(health);
			}
		}

		public int MaxHealth{ get => maxHealth; }
		public Vector3 TankVelocity { get => tankVelocity; }

		private void Awake()
		{
			tankRigidbody = GetComponent<Rigidbody>();
			inputReader.attackEvent += OnAttack;
			inputReader.nextWeaponEvent += OnNextWeapon;
			inputReader.previousWeaponEvent += OnPreviousWeapon;
			inputReader.movementEvent += OnMovement;
			inputReader.mouseAimEvent += OnMouseAim;
		}

		private void Start()
		{
			SwitchToNextWeapon(); 
		}

		private void OnEnable()
		{
			Alive = true;
			Health = maxHealth;
		}

		private void FixedUpdate()
		{
			if (Alive)
			{
				MovementUpdate();
			}
		}

		private void OnDestroy()
		{
			inputReader.attackEvent -= OnAttack;
			inputReader.nextWeaponEvent -= OnNextWeapon;
			inputReader.previousWeaponEvent -= OnPreviousWeapon;
			inputReader.movementEvent -= OnMovement;
			inputReader.mouseAimEvent -= OnMouseAim;
		}

		public void ApplyDamage(int damageAmount)
		{
			if (Alive)
			{
				Health = Mathf.Max(Health - Mathf.FloorToInt(damageAmount * armour), 0);
				if (Health == 0)
				{
					Alive = false;
					died?.Invoke(this);
				}
			}
		}

		public void OnMovement(Vector2 directionVector)
		{
			ControlVector = directionVector;
		}

		public void OnAttack()
		{
			turrets[currentTurret].Shoot(this);
		}

		public void OnNextWeapon()
		{
			SwitchToNextWeapon();
		}

		public void OnPreviousWeapon()
		{
			SwitchToNextWeapon();
		}

		public void OnMouseAim(Vector2 mousePos)
		{
			var c = Camera.main.transform.position;
			var mousePosition = mousePos;
			var m = Camera.main.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, 100.0f));
			float t = Mathf.InverseLerp(c.y, m.y, turretsHolder.position.y);
			var clickPosition = Vector3.Lerp(c, m, t);

			turretsHolder.LookAt(clickPosition);
		}

		private void SwitchToNextWeapon()
		{
			currentTurret = (currentTurret + 1) % turrets.Count;
			for (int i = 0; i < turrets.Count; i++)
			{
				turrets[i].SetState(i == currentTurret);
			}
		}

		private void MovementUpdate()
		{
			if (ControlVector.magnitude > 0.01f)
			{
				tankRigidbody.MoveRotation(Quaternion.Euler(0, Quaternion.FromToRotation(Vector3.forward, new Vector3(ControlVector.x, 0, ControlVector.y)).eulerAngles.y, 0));
				tankVelocity = transform.forward * ControlVector.magnitude * movementSpeed;
				Vector3 movement = tankVelocity * Time.deltaTime;
				tankRigidbody.MovePosition(tankRigidbody.position + movement);
			}
			else
			{
				tankVelocity = Vector3.zero;
			}
		}
	}
}