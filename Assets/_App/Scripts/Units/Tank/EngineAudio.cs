﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
	public class EngineAudio : MonoBehaviour
	{
		private Tank owner;
		[SerializeField]
		private AudioSource engineAudioSource = null;
		[SerializeField]
		private AudioClip engineIdleClip = null;
		[SerializeField]
		private AudioClip engineDrivingClip = null;

		private float originalPitch;
		private float pitchRange = 0.2f;
		private float moveThreshold = 0.1f;

		private void Awake()
		{
			owner = GetComponentInParent<Tank>();
			originalPitch = engineAudioSource.pitch;
		}

		private void Update()
		{
			if (!owner.Alive)
			{
				if (engineAudioSource.isPlaying)
				{
					engineAudioSource.Stop();
				}
				return;
			}
			if (Mathf.Abs(owner.ControlVector.y) < moveThreshold
				&& Mathf.Abs(owner.ControlVector.x) < moveThreshold)
			{
				if (engineAudioSource.clip != engineIdleClip)
				{
					engineAudioSource.clip = engineIdleClip;
					engineAudioSource.pitch = originalPitch + Random.Range(-pitchRange, pitchRange);
					engineAudioSource.Play();
				}
			}
			else
			{
				if (engineAudioSource.clip != engineDrivingClip)
				{
					engineAudioSource.clip = engineDrivingClip;
					engineAudioSource.pitch = originalPitch + Random.Range(-pitchRange, pitchRange);
					engineAudioSource.Play();
				}
			}
		}
	}
}