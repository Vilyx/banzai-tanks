﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
	public class TankTurret : MonoBehaviour
	{
		[SerializeField]
		private GameObject shellPrefab = null;
		[SerializeField]
		private Transform barrel = null;
		[SerializeField]
		private float shellStartForce = 1000;
		[SerializeField]
		private float reloadDuration = 3;

		private Animator animator = null;
		private float lastShotTime = 0;

		private void Awake()
		{
			animator = GetComponent<Animator>();
		}

		public void Shoot(Tank owner)
		{
			if (Time.time - lastShotTime > reloadDuration)
			{
				lastShotTime = Time.time;
				var shellGo = Instantiate(shellPrefab);
				shellGo.transform.position = barrel.position;
				shellGo.transform.rotation = barrel.rotation;
				var shellRb = shellGo.GetComponent<Rigidbody>();
				shellRb.velocity = owner.TankVelocity;
				shellRb.AddForce(shellGo.transform.forward * shellStartForce);
				var shellScript = shellGo.GetComponent<IShell>();
				shellScript.Owner = owner;
			}
		}

		public void SetState(bool visible)
		{
			lastShotTime = Time.time;
			animator.SetBool("Visible", visible);
		}
	}
}