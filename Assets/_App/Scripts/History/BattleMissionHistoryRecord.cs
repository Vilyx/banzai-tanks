﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App
{
	public struct BattleMissionHistoryRecord
	{
		public float timestamp;
		public HistoryRecordType recordType;

		public BattleMissionHistoryRecord(HistoryRecordType recordType, float timestamp)
		{
			this.recordType = recordType;
			this.timestamp = timestamp;
		}
	}
}
