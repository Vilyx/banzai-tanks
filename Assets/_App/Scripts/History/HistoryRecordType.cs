﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App
{
	public enum HistoryRecordType
	{
		GAME_STARTED = 0,
		ENEMY_KILLED = 1,
		ENEMY_DIED = 2,
	}
}
