﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
	public class BattleHistory
	{
		public static List<BattleMissionHistoryRecord> HistoryRecords { get; } = new List<BattleMissionHistoryRecord>();
		public static float BattleTime
		{
			get
			{
				return Time.time - battleStartTime;
			}
		}

		private static float battleStartTime;

		public static void Clear()
		{
			HistoryRecords.Clear();
		}

		public static void AddRecord(HistoryRecordType recordType)
		{
			float timestamp = 0;
			if (recordType == HistoryRecordType.GAME_STARTED)
			{
				battleStartTime = Time.time;
				timestamp = battleStartTime;
			}
			else
			{
				timestamp = Time.time - battleStartTime;
			}
			var record = new BattleMissionHistoryRecord(recordType, timestamp);
			HistoryRecords.Add(record);
		}
	} 
}
