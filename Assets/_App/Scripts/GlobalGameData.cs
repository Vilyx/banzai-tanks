﻿using System;
using UnityEngine;

namespace App
{
	public class GlobalGameData
	{
		private static GlobalGameData instance;
		private int completedMissions = 0;
		private int currentMissionIndex = 0;

		public static GlobalGameData Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new GlobalGameData();
				}
				return instance;
			}
		}

		public int CompletedMissions
		{
			get => completedMissions; set
			{
				completedMissions = value;
				PlayerPrefs.SetInt("GlobalGameData::CompletedMissions", completedMissions);
			}
		}
		public int CurrentMissionIndex
		{
			get => currentMissionIndex; set
			{
				currentMissionIndex = value;
				PlayerPrefs.SetInt("GlobalGameData::CurrentMissionIndex", currentMissionIndex);
			}
		}

		public void ResetGame()
		{
			completedMissions = 0;
			PlayerPrefs.SetInt("GlobalGameData::CompletedMissions", 0);
			currentMissionIndex = 0;
			PlayerPrefs.SetInt("GlobalGameData::CurrentMissionIndex", 0);
		}

		private GlobalGameData()
		{
			completedMissions = PlayerPrefs.GetInt("GlobalGameData::CompletedMissions", 0);
			currentMissionIndex = PlayerPrefs.GetInt("GlobalGameData::CurrentMissionIndex", 0);
		}
	}
}