using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace App
{
	public class MainMenuUI : MonoBehaviour
	{
		[SerializeField]
		private GlobalGameData globalGameData = null;
		[SerializeField]
		private GameObject newGameButtons;
		[SerializeField]
		private GameObject continuePlayingButtons;

		private void Awake()
		{
			globalGameData = GlobalGameData.Instance;
			if (globalGameData.CompletedMissions > 0)
			{
				newGameButtons.SetActive(false);
				continuePlayingButtons.SetActive(true);
			}
			else
			{
				continuePlayingButtons.SetActive(false);
				newGameButtons.SetActive(true);
			}
		}

		public void StartNewGame()
		{
			FindObjectOfType<SceneLoadController>().OpenLevel(0);
		}

		public void ContinuePlaying()
		{
			FindObjectOfType<SceneLoadController>().OpenLevel(globalGameData.CompletedMissions);
		}

		public void OpenLevel(int level)
		{
			FindObjectOfType<SceneLoadController>().OpenLevel(level);
		}

		public void ResetGame()
		{
			globalGameData.ResetGame();
		}
	} 
}
