﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace App
{
	public class BattleManager : MonoBehaviour
	{
		public event Action<Tank> tankSpawned;
		public event Action playerWon;
		public event Action playerLost;

		[SerializeField]
		private GameObject defaultMissionPrefab = null;
		[SerializeField]
		private CinemachineVirtualCamera virtualCamera = null;
		private IBattleMission battleMission = null;
		private TankSpawner tankSpawner;

		private void Awake()
		{
			tankSpawner = FindObjectOfType<TankSpawner>();
		}

		private async void Start()
		{
			var sceneLoader = FindObjectOfType<SceneLoadController>();
#if UNITY_EDITOR
			while (sceneLoader == null)
			{
				await Task.Delay(25);
				sceneLoader = FindObjectOfType<SceneLoadController>();
			}
#endif
			while (sceneLoader.CurrentState != SceneLoadController.State.COMPLETE)
			{
				await Task.Delay(25);
			}
			Debug.Log("Battle scene started");
			//Найти CrossSceneDataHolder и заспавнить из него battleMissionPrefab
			var crossSceneData = FindObjectOfType<CrossSceneDataHolder>();
			if (crossSceneData != null)
			{
				battleMission = Instantiate(crossSceneData.battleMissionPrefab, transform).GetComponent<IBattleMission>();
				Destroy(crossSceneData.gameObject);
			}
			else
			{
				battleMission = Instantiate(defaultMissionPrefab, transform).GetComponent<IBattleMission>();
			}
			StartBattle();
		}

		private void OnDestroy()
		{
			battleMission.missionDone -= OnMissionDone;
		}

		public void StartBattle()
		{
			BattleHistory.Clear();
			BattleHistory.AddRecord(HistoryRecordType.GAME_STARTED);

			var spawnedTank = tankSpawner.Spawn();
			virtualCamera.LookAt = spawnedTank.transform;
			virtualCamera.Follow = spawnedTank.transform;
			var tankScript = spawnedTank.GetComponent<Tank>();
			tankScript.died += OnTankDied;
			tankSpawned?.Invoke(tankScript);

			battleMission.StartMission();
			battleMission.missionDone += OnMissionDone;
		}

		private void OnTankDied(IDamageable tank)
		{
			playerLost?.Invoke();
			Time.timeScale = 0;
		}

		private void OnMissionDone()
		{
			battleMission.missionDone -= OnMissionDone;
			BattleHistory.Clear();
			playerWon?.Invoke();
			Time.timeScale = 0;
		}
	}
}