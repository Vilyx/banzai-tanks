﻿using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace App
{
	public class EditorColdStarter : MonoBehaviour
	{
#if UNITY_EDITOR
		[SerializeField]
		private SceneField persistentSceneReference;

		private void Start()
		{
			Scene persistentScene = SceneManager.GetSceneByName(persistentSceneReference.SceneName);
			if (!persistentScene.isLoaded)
			{
				SceneManager.LoadSceneAsync(persistentSceneReference.SceneName, LoadSceneMode.Additive);
			}
		}
#endif
	}
}