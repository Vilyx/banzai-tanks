﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
	[System.Serializable]
	public class ScenarioItemEnemySpawn
	{
		[SerializeField]
		private EnemySpawner enemySpawner = null;
		[SerializeField]
		private float time = 0;

		public float Time { get => time; internal set => time = value; }

		public void Invoke()
		{
			enemySpawner.Spawn();
		}
	}
}
