﻿using System;

namespace App
{
	public interface IBattleMission
	{
		event Action missionDone;

		void StartMission();
	}
}