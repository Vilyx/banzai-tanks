﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
	public class BattleMissionWinCondition : MonoBehaviour
	{
		[SerializeField]
		private int enemiesToKill = 0;

		public bool IsDone()
		{
			var records = BattleHistory.HistoryRecords;
			int enemiesKilled = 0;
			for (int i = 0; i < records.Count; i++)
			{
				var record = records[i];
				if (record.recordType == HistoryRecordType.ENEMY_KILLED)
				{
					enemiesKilled++;
				}
			}
			return (enemiesKilled >= enemiesToKill);			
		}
	} 
}
