﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace App
{
	public class ScenariolessBattleMission : MonoBehaviour, IBattleMission
	{
		public event Action missionDone;

		[SerializeField]
		private BattleMissionWinCondition winCondition = null;
		[SerializeField]
		private int maxSimultaneousEnemies = 10;
		[Tooltip("Минимальное расстояние от танка достаточное чтобы враги спавнились вне экрана")]
		[SerializeField]
		private float minSpawnDistance = 18;

		private List<GameObject> spawnedEnemies = new List<GameObject>();
		private List<EnemySpawner> enemySpawners;
		private bool missionStarted = false;
		private Tank tank;

		public Tank Tank
		{
			get
			{
				if (tank == null)
				{
					tank = FindObjectOfType<Tank>();
				}
				return tank;
			}
		}

		private void Awake()
		{
			enemySpawners = FindObjectsOfType<EnemySpawner>().ToList();
		}

		private void Update()
		{
			if (missionStarted)
			{
				CheckWinConditions();
				AttemptToSpawn();
			}
		}

		public void StartMission()
		{
			missionStarted = true;
		}

		private void CheckWinConditions()
		{
			if (winCondition.IsDone())
			{
				missionDone?.Invoke();
			}
		}

		private void AttemptToSpawn()
		{
			spawnedEnemies.RemoveAll(item => item == null);
			if (spawnedEnemies.Count > maxSimultaneousEnemies)
			{
				return;
			}
			EnemySpawner randomSpawner;
			float distanceToTank;
			do
			{
				int randomIndex = Random.Range(0, enemySpawners.Count);
				randomSpawner = enemySpawners[randomIndex];
				distanceToTank = (Tank.transform.position - randomSpawner.transform.position).magnitude;
			} while (distanceToTank < minSpawnDistance);

			spawnedEnemies.Add(randomSpawner.Spawn());
		}
	}
}
