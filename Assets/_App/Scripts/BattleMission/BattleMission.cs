﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
	public class BattleMission : MonoBehaviour, IBattleMission
	{
		public event Action missionDone;
		[SerializeField]
		private List<ScenarioItemEnemySpawn> missionScenario = null;
		[SerializeField]
		private BattleMissionWinCondition winCondition = null;

		private int currentEvent = 0;
		private bool missionStarted = false;
		private float missionStartTime = 0;

		private void Update()
		{
			if (missionStarted)
			{
				InvokeMissionEvents();
				CheckWinConditions();
			}
		}

		public void StartMission()
		{
			missionStarted = true;
			missionStartTime = Time.time;
		}

		private void InvokeMissionEvents()
		{
			float timeFromMissionStart = (Time.time - missionStartTime);
			if (currentEvent < missionScenario.Count
				  && missionScenario[currentEvent].Time < timeFromMissionStart)
			{
				missionScenario[currentEvent].Invoke();
				currentEvent++;
			}
		}

		private void CheckWinConditions()
		{
			if (winCondition.IsDone())
			{
				missionDone?.Invoke();
			}
		}
	}
}
