﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace App
{
	public class SceneLoadController : MonoBehaviour
	{
		public enum State { 
			LOADING, COMPLETE
		}

		private GlobalGameData globalGameData = null;
		[SerializeField]
		private List<GameObject> battleMissionPrefabs = null;
		[SerializeField]
		private SceneField battleSceneReference;
		[SerializeField]
		private SceneField mainMenuSceneReference;

		private Scene battleScene;
		private Scene mainMenuScene;

		private Scene currentLoadedScene;

		public State CurrentState { get; private set; } = State.COMPLETE;

		private void Awake()
		{
			globalGameData = GlobalGameData.Instance;

#if !UNITY_EDITOR
			CurrentState = State.LOADING;
			SceneManager.LoadSceneAsync(mainMenuSceneReference.SceneName, LoadSceneMode.Additive).completed += OnMainSceneLoaded;
#endif
		}

		public void OpenNextLevel()
		{
			OpenLevel(globalGameData.CurrentMissionIndex + 1);
		}

		public void RelaunchCurrentLevel()
		{
			OpenLevel(globalGameData.CurrentMissionIndex);
		}

		public void OpenLevel(int missionIndex)
		{
			globalGameData.CurrentMissionIndex = missionIndex;

			UnloadCurrentScene();

			CurrentState = State.LOADING;
			SceneManager.LoadSceneAsync(battleSceneReference.SceneName, LoadSceneMode.Additive).completed += OnBattleSceneLoaded;
		}

		public void OpenMainMenuScene()
		{
			UnloadCurrentScene();

			CurrentState = State.LOADING;
			SceneManager.LoadSceneAsync(mainMenuSceneReference.SceneName, LoadSceneMode.Additive).completed += OnMainSceneLoaded;
		}

		private void OnBattleSceneLoaded(AsyncOperation obj)
		{
			if (battleScene == null || battleScene.name == null)
			{
				battleScene = SceneManager.GetSceneByName(battleSceneReference.SceneName);
			}

			currentLoadedScene = battleScene;
			obj.completed -= OnBattleSceneLoaded;

			SceneManager.SetActiveScene(currentLoadedScene);
			CurrentState = State.COMPLETE;

			var crossSceneDataHolder = new GameObject("CrossSceneDataHolder");
			var data = crossSceneDataHolder.AddComponent<CrossSceneDataHolder>();
			data.battleMissionPrefab = battleMissionPrefabs[globalGameData.CurrentMissionIndex];

			Time.timeScale = 1;
		}

		private void OnMainSceneLoaded(AsyncOperation obj)
		{
			if (mainMenuScene == null || mainMenuScene.name == null)
			{
				mainMenuScene = SceneManager.GetSceneByName(mainMenuSceneReference.SceneName);
			}

			currentLoadedScene = mainMenuScene;

			SceneManager.SetActiveScene(currentLoadedScene);
			CurrentState = State.COMPLETE;

			obj.completed -= OnMainSceneLoaded;
		}

		private void UnloadCurrentScene()
		{
			if (!currentLoadedScene.isLoaded)
			{
				SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
			}
			else
			{
				SceneManager.UnloadSceneAsync(currentLoadedScene);
			}
		}
	}
}