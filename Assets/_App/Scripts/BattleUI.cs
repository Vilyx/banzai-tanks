﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace App
{
	public class BattleUI : MonoBehaviour
	{
		[SerializeField]
		private GlobalGameData globalGameData = null;

		[SerializeField]
		private GameObject fightScreen = null;
		[SerializeField]
		private GameObject winScreen = null;
		[SerializeField]
		private GameObject loseScreen = null;
		[SerializeField]
		private Animator countdown = null;
		[SerializeField]
		private Slider healthBar = null;

		private BattleManager battleManager;

		private Tank boundTank;

		private void Awake()
		{
			globalGameData = GlobalGameData.Instance;
			battleManager = FindObjectOfType<BattleManager>();
			battleManager.tankSpawned += OnTankSpawned;
			battleManager.playerWon += OnPlayerWon;
			battleManager.playerLost += OnPlayerLost;
			StartCoroutine(ShowCountdown());
		}

		public void LaunchNextLevel()
		{
			FindObjectOfType<SceneLoadController>().OpenNextLevel();
		}

		public void RelaunchCurrentLevel()
		{
			FindObjectOfType<SceneLoadController>().RelaunchCurrentLevel();
		}

		public void QuitToMainMenu()
		{
			FindObjectOfType<SceneLoadController>().OpenMainMenuScene();
		}

		private IEnumerator ShowCountdown()
		{
			countdown.gameObject.SetActive(true);
			yield return new WaitForSeconds(3);
			countdown.gameObject.SetActive(false);
		}

		private void OnTankHealthChanged(int newValue)
		{
			healthBar.value = Mathf.Clamp01((float)newValue / boundTank.MaxHealth);
		}

		private void OnTankSpawned(Tank tank)
		{
			UnsubscribeFromBoundTank();
			boundTank = tank;
			tank.healthChanged += OnTankHealthChanged;
			OnTankHealthChanged(tank.Health);
		}

		private void UnsubscribeFromBoundTank()
		{
			if (boundTank != null)
			{
				boundTank.healthChanged -= OnTankHealthChanged;
			}
		}

		private void OnPlayerLost()
		{
			fightScreen.SetActive(false);
			loseScreen.SetActive(true);
		}

		private void OnPlayerWon()
		{
			if (globalGameData.CompletedMissions < globalGameData.CurrentMissionIndex + 1)
			{
				globalGameData.CompletedMissions = globalGameData.CurrentMissionIndex + 1;
			}
			fightScreen.SetActive(false);
			winScreen.SetActive(true);
		}

	}
}