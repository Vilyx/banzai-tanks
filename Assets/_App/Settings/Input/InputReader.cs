using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;


namespace App
{
	[CreateAssetMenu(fileName = "InputReader", menuName = "Game/Input Reader")]
	public class InputReader : ScriptableObject, TankControls.IBattleActions
	{
		public event UnityAction attackEvent = delegate { };
		public event UnityAction nextWeaponEvent = delegate { };
		public event UnityAction previousWeaponEvent = delegate { };
		public event UnityAction<Vector2> mouseAimEvent = delegate { };
		public event UnityAction<Vector2> movementEvent = delegate { };

		private TankControls tankControls;

		private void OnEnable()
		{
			if (tankControls == null)
			{
				tankControls = new TankControls();
				tankControls.Battle.SetCallbacks(this);
			}

			EnableBattleInput();
		}

		private void OnDisable()
		{
			DisableAllInput();
		}

		public void OnMovement(InputAction.CallbackContext context)
		{
			movementEvent.Invoke(context.ReadValue<Vector2>());
		}

		public void OnAttack(InputAction.CallbackContext context)
		{
			if (context.phase == InputActionPhase.Performed)
				attackEvent.Invoke();
		}

		public void OnNextWeapon(InputAction.CallbackContext context)
		{
			if (context.phase == InputActionPhase.Performed)
				nextWeaponEvent.Invoke();
		}

		public void OnPreviousWeapon(InputAction.CallbackContext context)
		{
			if (context.phase == InputActionPhase.Performed)
				previousWeaponEvent.Invoke();
		}

		public void OnMouseAim(InputAction.CallbackContext context)
		{
			mouseAimEvent.Invoke(context.ReadValue<Vector2>());
		}

		public void EnableBattleInput()
		{
			tankControls.Battle.Enable();
		}

		public void DisableAllInput()
		{
			tankControls.Battle.Disable();
		}
	}
}